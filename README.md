# eslint-plugin-mui-make-styles

eslint plugin to detect unused classes in material-ui&#39;s makeStyles and to detect references to non-existant styles used within your code

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-mui-make-styles`:

```
$ npm install eslint-plugin-mui-make-styles --save-dev
```


## Usage

Add `mui-make-styles` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "mui-make-styles"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "mui-make-styles/rule-name": 2
    }
}
```

## Supported Rules

* Fill in provided rules here





